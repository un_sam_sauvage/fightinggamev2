﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class CharacterSelection : MonoBehaviour
{
    public GameObject gears;

    public GameObject characterPortrait;

    public myArray[] selct = new myArray[2];

    public int maxX;
    public int maxY;
    
    public int lenghtX;
    public int lengthY;

    public GameObject player1Selection;
    public GameObject player2Selection;

    public GameObject showCharacterRight;
    public Vector3 rectShowCharacterRight;
    public GameObject showCharacterLeft;

    public List<GameObject> evryThing = new List<GameObject>();

    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        
        showCharacterRight.transform.SetParent(gears.GetComponent<Gears>().canvasMain.transform);
        showCharacterRight.GetComponent<RectTransform>().localPosition = rectShowCharacterRight;
        evryThing.Add(showCharacterRight);
        
        showCharacterLeft.transform.SetParent(gears.GetComponent<Gears>().canvasMain.transform);
        showCharacterLeft.GetComponent<RectTransform>().localPosition = new Vector3(-rectShowCharacterRight.x, 0,0);
        evryThing.Add(showCharacterLeft); //setup l'objet pour le perso à gauche et à droite(showcase)

        GameObject p2 = Instantiate(player2Selection);
        evryThing.Add(p2);
        p2.GetComponent<PlayerSelection>().characterSelection = gameObject;
        p2.transform.SetParent(gears.GetComponent<Gears>().canvasMain.transform);
        player2Selection = p2;
        
        GameObject p1 = Instantiate(player1Selection);
        evryThing.Add(p1);
        p1.GetComponent<PlayerSelection>().player1 = true;
        p1.GetComponent<PlayerSelection>().characterSelection = gameObject;
        p1.transform.SetParent(gears.GetComponent<Gears>().canvasMain.transform);
        player1Selection = p1; //setup l'objet pour la selection du joueur 1 et 2
        
        //transform.SetParent(gears.GetComponent<Gears>().canvasMain.transform);
        
        maxX = 1; //selectGrid.Length - 1;
        maxY = 1;

        lenghtX = selct[0].ele.Length; 
        lengthY = selct.Length;

        int i = 0;

        foreach (var rows in selct) //créer la grille de selection
        {
            int a = 0;
            foreach (var lele in rows.ele)
            {
                rows.ele[a] = gears.GetComponent<Gears>().characters[a + lenghtX * i];
                
                GameObject go = Instantiate(characterPortrait, rows.ele[a].posOnScreenSelection, characterPortrait.transform.rotation);//affichage
                evryThing.Add(go);
                go.transform.SetParent(gears.GetComponent<Gears>().canvasMain.transform);
                go.GetComponent<RectTransform>().localPosition = rows.ele[a].posOnScreenSelection;
                go.GetComponent<Image>().sprite = rows.ele[a].mySpriteOnScreenSelection;
                
                a++;
            }
            i++;
        }
    }
    
    void Update()
    {
        if (player1Selection.GetComponent<PlayerSelection>().selected && //si les deux joueurs ont choisi lancer la game
            player2Selection.GetComponent<PlayerSelection>().selected)
        {
            Reset();
            gears.GetComponent<Gears>().LoadGame();
        }
    }

    public void Reset() //tous détruire la grille
    {
        foreach (var obj in evryThing)
        {
            Destroy(obj);
        }
    }

    public void UpdateCharacter(CharacterSelect sel, bool p1) //montrter le character showcase
    {
        if (!p1)
        {
            showCharacterRight.GetComponent<UnityEngine.UI.Image>().sprite = sel.showCase;
        }
        else
        {
            showCharacterLeft.GetComponent<UnityEngine.UI.Image>().sprite = sel.showCase;
        }
    }
}

[System.Serializable]
public class myArray
{
    public CharacterSelect[] ele = new CharacterSelect[2]; //pour faire un double tableau dans l'inspector
}