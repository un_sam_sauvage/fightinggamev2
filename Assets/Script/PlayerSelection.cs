﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerSelection : MonoBehaviour
{
    public bool player1;
    
    public GameObject characterSelection;
    
    public GameObject gears;
    
    public string[] inputs = new string[4];
    
    public CharacterSelect characterSelected;
    public int charSelectX;
    public int charSelectY;

    public bool selected;
    public Color baseColor;

    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        baseColor = GetComponent<UnityEngine.UI.Image>().color;
    }
    
    void Update()
    {
        if (!selected) //si ont n'a pas déja séléctioner
        {
            GetComponent<UnityEngine.UI.Image>().color = baseColor;
            
            if (Input.GetButtonDown(inputs[0])) //inputs = list de string, si l'input est appuyer se déplacer sur la grille vers le haut
            {
                if (charSelectY == characterSelection.GetComponent<CharacterSelection>().maxY)
                {
                    charSelectY = 0;   
                }
                else
                {
                    charSelectY++;
                }
            }
        
            if (Input.GetButtonDown(inputs[1])) //inputs = list de string, si l'input est appuyer se déplacer sur la grille vers la gauche
            {
                if (charSelectX == 0)
                {
                    charSelectX = characterSelection.GetComponent<CharacterSelection>().maxX;
                }
                else
                {
                    charSelectX--;
                }
            }
        
            if (Input.GetButtonDown(inputs[2])) //inputs = list de string, si l'input est appuyer se déplacer sur la grille vers le bas
            {
                if (charSelectY == 0)
                {
                    charSelectY = characterSelection.GetComponent<CharacterSelection>().maxY;   
                }
                else
                {
                    charSelectY--;
                }
            }

            if (Input.GetButtonDown(inputs[3])) //inputs = list de string, si l'input est appuyer se déplacer sur la grille vers la droite
            {
                if (charSelectX == characterSelection.GetComponent<CharacterSelection>().maxX)
                {
                    charSelectX = 0;   
                }
                else
                {
                    charSelectX++;
                }
            }
        }
        else //si ont à séléctioner
        {
            if (baseColor.r > 0) //se griser(feedback que la selection à été faite)
            {
                GetComponent<UnityEngine.UI.Image>().color = new Color(baseColor.r - 0.4f,0,0);
            }

            if (baseColor.g > 0)
            {
                GetComponent<UnityEngine.UI.Image>().color = new Color(0,baseColor.g - 0.4f,0);
            }
        }

        if (Input.GetButtonDown("Submit") && player1 || Input.GetButtonDown("Jump") && !player1) //si ont re séléctionne(déselectioner)
        {
            if (!selected)
            {
                if (player1)
                {
                    if (gears.GetComponent<Gears>().characterPlayer2 != characterSelected)
                    {
                        gears.GetComponent<Gears>().characterPlayer1 = characterSelected;
                        selected = !selected;
                    }
                }
                else
                { 
                    if (gears.GetComponent<Gears>().characterPlayer1 != characterSelected)
                    {
                        gears.GetComponent<Gears>().characterPlayer2 = characterSelected;
                        selected = !selected;
                    }
                }
            }
            else
            {
                selected = !selected;
            }
        }
        
        GetComponent<RectTransform>().localPosition = new Vector3(characterSelected //lock la position
            .posOnScreenSelection.x, characterSelected
            .posOnScreenSelection.y,
            -1);
            //characterSelected
                //.posOnScreenSelection; //Vector2.MoveTowards(transform.position,characterSelected.posOnScreenSelection, Time.deltaTime * 1);
    }

    void LateUpdate() //trouver le perso séléctionner et update l'affichage du perso
    {
        characterSelected = characterSelection.GetComponent<CharacterSelection>().selct[charSelectY].ele[charSelectX];
      
        characterSelection.GetComponent<CharacterSelection>().UpdateCharacter(characterSelected, player1);    
    }
}
