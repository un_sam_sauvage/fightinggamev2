﻿using UnityEngine;

public class AddForceP1 : MonoBehaviour
{
    public float pourcentage;
    public float poids;
    public GameObject healthBarP1;

    private void Start()
    {
        pourcentage = .1f;
        healthBarP1.GetComponent<HealthBar>().textUpdate(pourcentage);
        healthBarP1 = GameObject.FindGameObjectWithTag("HealthBarP1");
    }
    
    private void Update()
    {
        if (pourcentage <= .1f || pourcentage < 0)
        {
            pourcentage = .1f;
        }
    }
    public void GetTouch(float force, float direction)
    {    //cette fonction permet de repousser le joueur en fonction de l'attaque qu'a effectué l'adersaire
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-force*pourcentage/poids*direction, 2);
        pourcentage += pourcentage * force/100;
        //ici on change l'HUD en fonciton des dégâts pris
        if (GetComponent<AddForceP1>().enabled)
        {
            healthBarP1 = GameObject.FindGameObjectWithTag("HealthBarP1");
            healthBarP1.GetComponent<HealthBar>().textUpdate(pourcentage);
        }

    }
    public void Heal(float soin)
    {//retire des pourcentage et soigne le joueur
        pourcentage -= soin;
        if (pourcentage <= .1f || pourcentage <0)
        {
            pourcentage = .1f;
        }
        healthBarP1.GetComponent<HealthBar>().textUpdate(pourcentage);
    }
    
    public void LooseLife()
    {    //appelle la fonction qui fait perdre une vie au joueur

        healthBarP1.GetComponent<HealthBar>().textUpdate(0);
        healthBarP1.GetComponent<HealthBar>().OutOfScreen(true);
    }
}
