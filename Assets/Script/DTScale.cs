﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class DTScale : MonoBehaviour
{
    public Vector3 baseScale;

    public float scaleMagnitude;

    public float duration;
    
    
    void Start()
    {
        baseScale = transform.localScale; //prendre le scale de base de l'objet
        ScaleTO(); //appeler la fonction pour rescale l'objet qui va boucler avec celle qui scale au scale de base
    }
    
    void Update()
    {
        
    }

    public void ScaleTO()
    {
        transform.DOScale(baseScale * scaleMagnitude, duration).OnComplete(ScaleBase);
    }

    public void ScaleBase()
    {
        transform.DOScale(baseScale, duration).OnComplete(ScaleTO);
    }
}
