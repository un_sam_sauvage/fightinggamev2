﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    /// Variables ///
    public int maxHealth = 100;
    public int currentHealth;
    public HealthBar healthBar;
    //////
    
    // Start is called before the first frame update
    void Start()
    {
        //Initialise la variable à 0
        currentHealth = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Appelle la fonction TakeDamage en appuyant sur la barre espace
        if (Input.GetKeyDown(KeyCode.Space))
        {
            TakeDamage(5);
        }
    }

    // Fonction qui gère les dégats infligés au joueur
    void TakeDamage(int damage)
    {
        currentHealth += damage;

        healthBar.SetHealth(currentHealth);
    }
}
