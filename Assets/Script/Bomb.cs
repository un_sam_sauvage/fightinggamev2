﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public LayerMask whatIsEnnemies;
    public float force = 700f;
    
    public bool launched;
    private float countdown;
    public float radius;

    public GameObject owner;

    private Rigidbody2D rb;

    void Start()
    {
        launched = false;
        rb = GetComponent<Rigidbody2D>();
    }

    public void Update()
    {
        
            owner = transform.parent.gameObject;
        if (owner != null)
        {

            if (owner.layer == 12)
            {

                whatIsEnnemies = LayerMask.GetMask("Player2");
                Debug.Log("Player2");
            }

            if (owner.layer == 13)
            {
                whatIsEnnemies = LayerMask.GetMask("Player1");
                Debug.Log("Player1");
            }
        }
        
    }
    public void Explose()
    {
        Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(transform.position, radius, whatIsEnnemies); //trouver les ennemis aux alentours
        for (int i = 0; i < enemiesToDamage.Length; i++)
        {
            if (owner.GetComponent<MoveP1>().enabled)
            {
                Debug.Log("oui");
                enemiesToDamage[i].GetComponent<AddForceP2>().GetTouch(45, owner.GetComponent<MoveP1>().direction); //inflige des dégats à l'ennemis en récupérant son script
            }
            else if (owner.GetComponent<MoveP2>().enabled)
            {
                Debug.Log("oui2");
                enemiesToDamage[i].GetComponent<AddForceP1>().GetTouch(45, owner.GetComponent<MoveP2>().direction); //inflige des damage à l'ennemis en récupérant son script
            }
        }
        Destroy(gameObject);//détruit la grenade
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") && launched)
        {
            if (collision.gameObject.GetComponentInChildren<TakeItem>().enabled && !collision.gameObject.GetComponentInChildren<TakeItem>().GotItem)
            {
                Debug.Log("explose");
                Explose();
            }

            if (collision.gameObject.GetComponentInChildren<TakeItem1>().enabled && !collision.gameObject.GetComponentInChildren<TakeItem1>().GotItem)
            {
                Debug.Log("explose2");
                Explose();
            }

        }
    }
    private void OnDrawGizmosSelected() //pour voir les raycast
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
