﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn_meteor : MonoBehaviour
{
    public int nbmeteor = 5;
    public GameObject meteor;
    public float xmin, xmax, y;
    public float temps;
    
    void Start()
    {
        StartCoroutine(Spawn());
    }
    
    void Update()
    {
        if(nbmeteor <= 0)
            Destroy(gameObject);
    }

    IEnumerator Spawn()
    {
        Instantiate(meteor, new Vector3(Random.Range(xmin, xmax), y, 0), Quaternion.identity);
        yield return new WaitForSeconds(temps);
        nbmeteor --;
        if (nbmeteor > 0)
            StartCoroutine(Spawn());
    }
}
