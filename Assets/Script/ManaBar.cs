﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaBar : MonoBehaviour
{
    public Slider slider;
    public float mana; //maxMana;

    void Start()
    {
        slider.value = CalculateMana(); //Pour la barre de mana
    }
    
    float CalculateMana()
    {
        return mana;
    }

    public void DealDamage(float damageValue) //Permet de récupérer le nombre de dégats fait à l'adversaire
    {
        mana += damageValue;
        slider.value = CalculateMana();
    }

    public void ResetMana()
    {
        mana = 0;
        slider.value = CalculateMana();
    }
}
