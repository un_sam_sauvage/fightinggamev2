﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject gears;

    public GameObject[] toSpawn;

    public float timerB;
    public float timer;
    
    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
    }
    
    void Update()
    {
        timer -= Time.deltaTime; //timer

        if (timer <= 0) //si le timer est à 0
        {
            int i = Random.Range(0, toSpawn.Length);
            Instantiate(toSpawn[i], new Vector2(transform.position.x + Random.Range(-5, 5), transform.position.y), //pop un objet
                toSpawn[i].transform.rotation);
            timer = timerB;
        }
    }
}
