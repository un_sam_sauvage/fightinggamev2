﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowPlayers : MonoBehaviour
{
    public GameObject gears;

    public bool forPlayer1;
    
    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
    }
    
    
    void Update()
    {
        /*if (transform.parent.transform.localRotation == Quaternion.Euler(0,180,0))
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
            print("tes");
        }
        else
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }*/

        if (forPlayer1)
        {
            transform.position = new Vector3(0,3,0) + gears.GetComponent<Gears>().player1.transform.position;
        }
        else
        {
            transform.position = new Vector3(0,3,0) + gears.GetComponent<Gears>().player2.transform.position;
        }
    }
}
