﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonstreHealth : MonoBehaviour
{
    public float healthMonster;
    public float healthMonsterMax;
    public Rigidbody2D _Rigidbody2D;

    void Update()
    {
        if (healthMonster <= 0)
        {
            Destroy(gameObject); //meurt / detruit
        }
    }
    
    public void TakeDamage(int damage)
    {
        healthMonster -= damage;
    }

}