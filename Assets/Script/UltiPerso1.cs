﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UltiPerso1 : MonoBehaviour
{
    public LayerMask whatIsEnnemies;
    public int speed;
    public GameObject player;

    void Start()
    {
        player = GameObject.Find("Perso 1(Clone)"); //récupère le perso sur la scène

        if (player.layer == 12)
            whatIsEnnemies = LayerMask.GetMask("Player2"); //Dis quel layer est l'ennemis en fonction du layer qu'a perso

        if (player.layer == 13)
            whatIsEnnemies = LayerMask.GetMask("Player1");
    }

    void Update()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime); //fais bouger l'objet vers la droit
        
        Collider2D[] enemiesToDamage = Physics2D.OverlapBoxAll( GetComponent<Renderer>().bounds.center, new Vector2(4.5f,5.35f), 0f, whatIsEnnemies);//trouver les ennemis aux alentours
        for (int i = 0; i < enemiesToDamage.Length; i++)
        {
            if (player.GetComponent<MoveP1>().enabled)
            {
                Debug.Log("oui");
                enemiesToDamage[i].GetComponent<AddForceP2>().GetTouch(35,player.GetComponent<MoveP1>().direction); //inflige des dégats à l'ennemis en récupérant son script
            }
            else if (player.GetComponent<MoveP2>().enabled)
            { 
                Debug.Log("oui2");
                enemiesToDamage[i].GetComponent<AddForceP1>().GetTouch(35,player.GetComponent<MoveP2>().direction); //inflige des damage à l'ennemis en récupérant son script
            }
            
            Destroy(gameObject);
        }
    }
    
    private void OnBecameInvisible() //quand il sort de l'écran
    {
        Destroy(gameObject);
    }
    
    private void OnDrawGizmosSelected() //pour voir les raycast
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube( GetComponent<Renderer>().bounds.center, new Vector3(4.5f, 5.35f, 0));
    }
}
