﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YTest : MonoBehaviour
{
    public Rigidbody2D rb;

    public float percent;
    
    void Start()
    {
        
    }
    
    void Update()
    {
        
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        Vector2 directionTarget = other.gameObject.transform.position - transform.position;

        rb.velocity = directionTarget.normalized * percent;
    }
}
