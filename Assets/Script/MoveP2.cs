﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MoveP2 : MonoBehaviour
{
    public float speed;
    public bool isGrounded;

    public float jumpForce;
    private Rigidbody2D rb;

    public bool facingRight;
    private int facingDirection = 1;
    public float direction;

    public bool timer;
    public float cooldown;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        direction = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<MoveP2>().enabled)
        {
            //déplacement du joueur
            if (Input.GetKeyDown(KeyCode.UpArrow) && isGrounded) //mise en place du jump en fonction de la detection du sol
            {
                rb.velocity = Vector2.up * jumpForce * 1.5f;
                isGrounded = false;
            }

            if (Input.GetKey(KeyCode.DownArrow) && !isGrounded)
            {
                transform.Translate(Vector3.down * speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.Translate(Vector3.right * speed * Time.deltaTime);
                direction = -1;
                transform.localRotation = Quaternion.Euler(0, 0, 0);

            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(Vector3.right * speed * Time.deltaTime);
                direction = 1;
                transform.localRotation = Quaternion.Euler(0, 180, 0);
            }

            if (timer)
            {
                cooldown -= Time.deltaTime;
            }
            if (cooldown < 0)
            {
                cooldown = 4;
                speed = speed / 2;
                timer = false;
            }
            if (Mathf.Abs(rb.velocity.x) > 15)
            {
                isGrounded = false;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Potion"))
        {
            speed = speed *= 2;
            Destroy(other.gameObject);
            timer = true;

        }
    }


}

