﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UltiPerso3 : MonoBehaviour
{
    public LayerMask whatIsEnnemies;
    public LayerMask whatIsGround;
    public int damage;
    public float radius;
    public float speed;
    public GameObject player;
    
    private void Start()
    {
        //permet de récupérer si on est le joueur 1 ou 2
        player = GameObject.Find("Perso 3(Clone)");

        if (player.layer == 12) 
            whatIsEnnemies = LayerMask.GetMask("Player2");

        if (player.layer == 13)
            whatIsEnnemies = LayerMask.GetMask("Player1");
    }

    void Update()
    {
        transform.Translate(Vector2.down * speed * Time.deltaTime); //se déplace vers le bas

        
        Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(transform.position, radius, whatIsEnnemies); //trouve l'adversaire
        for (int i = 0; i < enemiesToDamage.Length; i++)
        {
            if (player.GetComponent<MoveP1>().enabled)
            {
                enemiesToDamage[i].GetComponent<AddForceP2>().GetTouch(35,player.GetComponent<MoveP1>().direction); //inflige des dégats à l'ennemis en récupérant son script
            }
            else if (player.GetComponent<MoveP2>().enabled)
            { 
                enemiesToDamage[i].GetComponent<AddForceP1>().GetTouch(35,player.GetComponent<MoveP2>().direction); //inflige des damage à l'ennemis en récupérant son script
            } 
            Destroy(gameObject);
        }
        
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, radius, whatIsGround); //trouve le sol
        for (int i = 0; i < collider.Length; i++)
        {
            Destroy(gameObject);
        }
    }
    
    private void OnBecameInvisible() //quand il sort de l'écran
    {
        Destroy(gameObject); //se détruit
    }
    
    private void OnDrawGizmosSelected() //pour voir les raycast
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
