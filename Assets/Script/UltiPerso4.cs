﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class UltiPerso4 : MonoBehaviour
{    public GameObject player;
    public int nbPlayer;

    public void Start()
    {
        //permet de récupérer si on est le joueur 1 ou 2
        player = GameObject.Find("Perso 4(Clone)");

        if (player.layer == 12)
            nbPlayer = 2;

        if (player.layer == 13)
            nbPlayer = 1;
    }

    public void Update()
    {
        if (nbPlayer == 2)
        {
            player.GetComponent<AddForceP1>().Heal(Random.Range(0.5f, 1)); //enlève des dégâts
        }
        
        if (nbPlayer == 1)
        {
            player.GetComponent<AddForceP2>().Heal(Random.Range(0.5f, 1)); //enlève des dégats
        }
        
        Destroy(gameObject);
    }
}