using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimiteTerrain : MonoBehaviour
{
    private Vector2 _screenBounds;
    // Start is called before the first frame update
    void Start()
    {
        _screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }

    // Update is called once per frame
    void Update()
    {
        float posX = transform.position.x;
        float posY = transform.position.y;
        if (posX < _screenBounds.x * -1 - 5 || posX > _screenBounds.x + 5 || posY < _screenBounds.y * -1 - 5 ||
            posY > _screenBounds.y + 5)
        {
            Debug.Log("out");
        }
    }
}
