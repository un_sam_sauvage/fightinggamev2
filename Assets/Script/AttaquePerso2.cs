﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AttaquePerso2 : MonoBehaviour
{
    public Transform attackP, attackE;
    public Transform firePos;
    public LayerMask whatIsEnnemies;
    public float attackRange;
    public float direction;

    public GameObject ultime;
    public GameObject manaBarP2;

    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();  //récupère le composant animation
        
        whatIsEnnemies = LayerMask.GetMask("Player1");
        
        manaBarP2 = GameObject.FindGameObjectWithTag("ManaBarP2");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad7)) //input A fait une attaque avec l'épée
        {
            anim.SetTrigger("AttaqueE"); //joue l'animation "coup d'épée"
            Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackE.position, attackRange,whatIsEnnemies); //un raycast circulaire qui prends en compte le layer correspondant à l'ennemis
            for (int i = 0; i < enemiesToDamage.Length; i++)
            {
                if (enemiesToDamage[i].CompareTag("Player"))
                {
                    enemiesToDamage[i].GetComponent<AddForceP1>().GetTouch(Random.Range(13, 16), GetComponent<MoveP2>().direction);//inflige des damage à l'ennemis en récupérant son script
                    manaBarP2.GetComponent<ManaBar>().DealDamage(20);
                } 
            }
        } 
        
        if (Input.GetKeyDown(KeyCode.Keypad8)) //Input E fait une attaque avec le pied
        {
            anim.SetTrigger("AttaqueP"); //joue l'animation "coup de pied"
            Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackP.position, attackRange,whatIsEnnemies);
            for (int i = 0; i < enemiesToDamage.Length; i++)
            {
                if (enemiesToDamage[i].CompareTag("Player"))
                {
                    enemiesToDamage[i].GetComponent<AddForceP1>().GetTouch(Random.Range(18, 22), GetComponent<MoveP2>().direction);
                    manaBarP2.GetComponent<ManaBar>().DealDamage(20);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Keypad9) && manaBarP2.GetComponent<ManaBar>().mana >= 100)  //lance l'ultime
        {
            anim.SetTrigger("AttaqueE"); //joue l'animation "coup d'épée"
            GameObject temp = Instantiate(ultime, transform.position ,Quaternion.identity);
            temp.transform.position = firePos.position;
            temp.transform.rotation = firePos.rotation;
            manaBarP2.GetComponent<ManaBar>().ResetMana();
        }
    }

    private void OnDrawGizmosSelected() //pour voir les raycast
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackP.position, attackRange);
        Gizmos.DrawWireSphere(attackE.position, attackRange);
    }
}
