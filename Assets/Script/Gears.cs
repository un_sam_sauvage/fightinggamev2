﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Gears : MonoBehaviour
{
    public string sceneToLoadName;

    public GameObject soundManager;
        
    public Camera cam;
    public Canvas canvasMain;
    public bool paused;
    public GameObject pausePanel;
    public GameObject menuPanel;

    public CharacterSelect[] characters;
    public CharacterSelect characterPlayer1;
    public CharacterSelect characterPlayer2;

    public GameObject player1;
    public GameObject showPlayer1;
    public GameObject player2;
    public GameObject showPlayer2;

    public GameObject fade;
    public GameObject winPanel;

    //public Map[] maps; //pour enlever le map manager
    public Map currentMap;
    
    void Start()
    {
        LoadMenu();
    }
    
    void Update()
    {
        //cam = Camera.current;

        if (Input.GetButtonDown("Cancel")) //si ont appui sur echape mettre en pause
        {
            Pause();
        }
    }

    public void LoadMenu() //charger le menu
    {
        SceneManager.LoadScene("MenuScene");
        winPanel.SetActive(false);
        menuPanel.SetActive(true);
        pausePanel.SetActive(false);
    }

    public void ReturnMenu() //retourner au menu depuis la pause
    {
        LoadMenu();
        Pause();
    }

    public void LoadCharacterSelectionScreen() //charger la scene de la selection de personnages
    {
        SceneManager.LoadScene("CharacterSelectionScene");
        menuPanel.SetActive(false);
    }
    
    public void LoadGame() //charger la scene du jeu
    {
        SceneManager.LoadScene(sceneToLoadName);
        
        fade.GetComponent<Image>().color = new Color(0,0,0,255);
        fade.GetComponent<Image>().DOFade(0, 0.5f);

        StartCoroutine("Test");
    }

    public IEnumerator Test() //appeller just après avoir selectionner les deux personnages sers à setup les deux personnages(côntrolls)
    {
        yield return new WaitForSeconds(0.5f);
        
        GameObject woo = Instantiate(characterPlayer1.toInstantiate);
        GameObject aah = Instantiate(characterPlayer2.toInstantiate);

        GameObject w = Instantiate(showPlayer1);//, woo.transform);
        //w.transform.localPosition = new Vector2(0, 5);
        w.GetComponent<ShowPlayers>().forPlayer1 = true;
        GameObject a = Instantiate(showPlayer2);//, aah.transform);
        //a.transform.localPosition = new Vector2(0, 5);

        player1 = woo;
        player2 = aah;
        
        player1.GetComponent<MoveP1>().enabled = true;
        player1.GetComponent<AttaquePerso>().enabled = true;
        player1.GetComponent<AddForceP1>().enabled = true;
        player1.GetComponentInChildren<TakeItem>().enabled = true;
        player1.GetComponent<MoveP2>().enabled = false;
        player1.GetComponent<AttaquePerso2>().enabled = false;
        player1.GetComponent<AddForceP2>().enabled = false;
        player1.GetComponentInChildren<TakeItem1>().enabled = false;
        player1.layer = 12;
     
        player2.GetComponent<MoveP2>().enabled = true;
        player2.GetComponent<AttaquePerso2>().enabled = true;
        player2.GetComponent<AddForceP2>().enabled = true;
        player2.GetComponentInChildren<TakeItem1>().enabled = true;
        player2.GetComponent<MoveP1>().enabled = false;
        player2.GetComponent<AttaquePerso>().enabled = false;
        player2.GetComponent<AddForceP1>().enabled = false;
        player2.GetComponentInChildren<TakeItem>().enabled = false;
        player2.layer = 13;

        player1.transform.position = currentMap.player1Spawn;
        player2.transform.position = currentMap.player2Spawn;
        
        cam.gameObject.GetComponent<YCamera>().GetPlayers();
        cam.gameObject.GetComponent<YCamera>().SetLimits(currentMap.minCam, currentMap.maxCam);
        cam.gameObject.transform.position = new Vector3(currentMap.centerOfTheStage.x, currentMap.centerOfTheStage.y, cam.gameObject.transform.position.z);
    }

    public void Pause() //mettre dès mettre la pause
    {
        if (!paused)
        {
            pausePanel.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            pausePanel.SetActive(false);
            Time.timeScale = 1;
        }

        paused = !paused;
    }

    public void Quit() //quitter l'application
    {
        Application.Quit();
    }
}

[System.Serializable]
public class CharacterSelect //contient les infos pour faire un personnages sur l'écran de sélection de personnages
{
    public Vector2 posOnScreenSelection;
    public Sprite mySpriteOnScreenSelection;
    public string myName;
    
    public GameObject toInstantiate;
    public Sprite showCase;
}

[System.Serializable]

public class Map
{
    public GameObject map;

    public Vector2 player1Spawn;
    public Vector2 player2Spawn;

    public Vector2 centerOfTheStage;

    public Vector2 minCam;
    public Vector2 maxCam;
}
