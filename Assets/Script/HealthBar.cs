﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthBar : MonoBehaviour
{
	/// VARIABLES ///
	public GameObject gears;
	
	public Slider slider;
	public Gradient gradient;
	public Image fill;
	public Image Heart1, Heart2, Heart3;
	public int heart;
	public TMP_Text pourcentageText;
	//////

	private void Start()
	{
		gears = GameObject.FindGameObjectWithTag("GameController");
		
		//Initialise la variable heart
		heart = 3;
		//Déclare que les objets Heart1, Heart2, Heart3 sont en état actifs
		Heart1.gameObject.SetActive(true);
		Heart2.gameObject.SetActive(true);
		Heart3.gameObject.SetActive(true);
	}

	private void Update()
	{
		//Condition permettant de ne pas dépasser 3 vies
		if(heart > 3)
		{
			heart = 3;
		}
		
		//Permet de switcher entre des états
		switch (heart)
		{
			//3 vies restantes
			case 3 :
				Heart1.gameObject.SetActive(true);
				Heart2.gameObject.SetActive(true);
				Heart3.gameObject.SetActive(true);
				break;
			
			//2 vies restantes
			case 2 :
				Heart1.gameObject.SetActive(true);
				Heart2.gameObject.SetActive(true);
				Heart3.gameObject.SetActive(false);
				break;
			
			//1 vie restante
			case 1 :
				Heart1.gameObject.SetActive(true);
				Heart2.gameObject.SetActive(false);
				Heart3.gameObject.SetActive(false);
				break;
			
			//0 vie restante
			case 0 :
				Heart1.gameObject.SetActive(false);
				Heart2.gameObject.SetActive(false);
				Heart3.gameObject.SetActive(false);
				break;
		}
	}

	public void OutOfScreen(bool player1)
	{
		if (player1)
		{
			heart -= 1;
		}
		else
		{
			heart -= 1;
		}
		
		if (heart == 0 && gameObject.CompareTag("HealthBarP1"))
		{
			gears.GetComponent<Gears>().winPanel.SetActive(true);
			gears.GetComponent<Gears>().winPanel.GetComponentInChildren<TextMeshProUGUI>().text = "PARTIE FINIE " + "LE JOUEUR 2 A GAGNE" ;
		}
		else if (heart == 0 && gameObject.CompareTag("HealthBarP2"))
		{
			gears.GetComponent<Gears>().winPanel.SetActive(true);
			gears.GetComponent<Gears>().winPanel.GetComponentInChildren<TextMeshProUGUI>().text = "PARTIE FINIE " + "LE JOUEUR 1 A GAGNE" ;
		}
	}


	/// Permet de lier la vie (donc les dégats reçus) et le slider
	public void SetMaxHealth(int health)
	{
		slider.maxValue = health;
		slider.value = health;

		fill.color = gradient.Evaluate(1f);
	}

    public void SetHealth(int health)
	{
		slider.value = health;

		fill.color = gradient.Evaluate(slider.normalizedValue);
	}
	//////
	
    //Permet d'actualiser le texte affichant le pourcentage de dégat
    public void textUpdate (float value)
    {
	    pourcentageText.text = Mathf.RoundToInt(value*100) + " %";
    }
}
