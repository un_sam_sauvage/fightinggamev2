﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YCamera : MonoBehaviour
{
    public GameObject gears;
    
    public Vector2 minCameraPos;
    public Vector2 maxCameraPos;

    public GameObject player1;
    public GameObject player2;

    public float maxDezoom;

    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
    }

    void Update()
    {
        if (gears.GetComponent<Gears>().player1) //si le gameManager a trouver le joueur 1(partie charger
        {
            float distancePlayers = Vector2.Distance( //trouver la distance entre la camera et le centre du terrain
                new Vector2(player1.transform.position.x, player1.transform.position.y),
                new Vector2(player2.transform.position.x, player2.transform.position.y));
            
                Vector2 posBetweenPlayers = //la position entre les deux joueur la camera doit suivre cette position pour avoir une vision des 2 joueurs
                    Vector2.Lerp(new Vector2(player1.transform.position.x, player1.transform.position.y),
                        new Vector2(player2.transform.position.x, player2.transform.position.y), 0.5f);

                Vector2 smoothedPosition = Vector2.Lerp(transform.position, posBetweenPlayers, 0.125f); //Smooth Camera

                transform.position = new Vector3(smoothedPosition.x, smoothedPosition.y, transform.position.z);

                /*if (distancePlayers >= 5)
                {
                    GetComponent<Camera>().orthographicSize = distancePlayers * 0.4f + 5;
                }*/
            
                GetComponent<Camera>().orthographicSize = distancePlayers * 0.4f + 5; //zoom/dézoom
                
                if (GetComponent<Camera>().orthographicSize > maxDezoom) //bloquer la camera a un certain dezoom(pour que les personnages puissent sortir de l'écran
                {
                    GetComponent<Camera>().orthographicSize = maxDezoom;
                }

                transform.position = new Vector3(Mathf.Clamp(transform.position.x, minCameraPos.x, maxCameraPos.x),
                    Mathf.Clamp(transform.position.y, minCameraPos.y, maxCameraPos.y),
                    transform.position.z); //limité la camera
            }
        
    }

    public void GetPlayers() //trouver les joueur appeller dans Gears
    {
        player1 = gears.GetComponent<Gears>().player1;
        player2 = gears.GetComponent<Gears>().player2;
    }

    public void SetLimits(Vector2 min, Vector2 max)
    {
        minCameraPos = min;
        maxCameraPos = max;
    }
}
