﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    public bool timer;

    public float ttimer;
    
    void Start()
    {
        if (timer) //si ont utilise un timer
        {
            Destroy(gameObject, ttimer);
        }
        else //sans timer
        {
            Destroy(gameObject);
        }
    }
}
