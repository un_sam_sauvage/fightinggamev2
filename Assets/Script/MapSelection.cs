﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSelection : MonoBehaviour
{
    public GameObject gears;
    // VARIABLES //
    public Map[] maps;
    //private int index;
    //////
    
    // Start is called before the first frame update
    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        
        //Instancie une map aléatoirement parmis celles présente dans le tableau
        Map theMap = maps[Random.Range(0, maps.Length)];
        Instantiate(theMap.map, new Vector3(0, 0, 0), Quaternion.identity);
        gears.GetComponent<Gears>().currentMap = theMap;
    }
}
