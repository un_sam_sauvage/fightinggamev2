﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UltiPerso2 : MonoBehaviour
{
    public LayerMask whatIsEnnemies;
    public int damage;
    public float force = 700f;
    
    public float delay = 3f;
    private float countdown;
    public bool hasExploded = false;
    public float radius;

    public GameObject player;
    
    void Start()
    {
        player = GameObject.Find("Perso 2(Clone)");
        Debug.Log(player.layer);
        
        if (player.layer == 12)
        {
            whatIsEnnemies = LayerMask.GetMask("Player2");
            Debug.Log("Player2");
        }

        if (player.layer == 13)
        {
            whatIsEnnemies = LayerMask.GetMask("Player1");
            Debug.Log("Player1");
        }

        countdown = delay;
    }

    void Update()
    {
        countdown -= Time.deltaTime;

        if (countdown <= 0 && !hasExploded) //une fois le timer arrivé à 0
        {
            Explose(); //la bombe explose
            hasExploded = true;
        }
    }

    public void Explose()
    { 
        Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(transform.position, radius, whatIsEnnemies); //trouver les ennemis aux alentours
       for (int i = 0; i < enemiesToDamage.Length; i++)
       {
           if (player.GetComponent<MoveP1>().enabled)
           {
               Debug.Log("oui");
               enemiesToDamage[i].GetComponent<AddForceP2>().GetTouch(45,player.GetComponent<MoveP1>().direction); //inflige des dégats à l'ennemis en récupérant son script
           }
           else if (player.GetComponent<MoveP2>().enabled)
           { 
               Debug.Log("oui2");
               enemiesToDamage[i].GetComponent<AddForceP1>().GetTouch(45,player.GetComponent<MoveP2>().direction); //inflige des damage à l'ennemis en récupérant son script
           }
       }
       Destroy(gameObject);//détruit la grenade
    }
    
    private void OnDrawGizmosSelected() //pour voir les raycast
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
