﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeItem : MonoBehaviour
{
    public GameObject player;
    public bool GotItem;
    public Transform objectPosition;
    public Transform launchPosition;


    // Start is called before the first frame update
    void Start()
    {
        GotItem = false;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        
        if (other.gameObject.CompareTag("Item")) 
        {
            
            if (GotItem == false && Input.GetKey(KeyCode.F))
            {

                Debug.Log("pris");

                GotItem = true;
                other.GetComponent<BoxCollider2D>().enabled = false;
                other.transform.parent = player.transform;
                other.GetComponent<Rigidbody2D>().isKinematic = true;
                other.transform.position = objectPosition.transform.position;

            }

            if (GotItem == true && Input.GetKey(KeyCode.G))
            {
                other.GetComponent<Bomb>().launched = true;
                other.transform.position = launchPosition.transform.position;
                Debug.Log("relacher");
                GotItem = false;
                other.transform.parent = null;
                other.GetComponent<Rigidbody2D>().isKinematic = false;
                other.GetComponent<Rigidbody2D>().AddForce(transform.right * 600.0f);
                other.GetComponent<BoxCollider2D>().enabled = true;

            }


        }  
        
        
    }   
}
