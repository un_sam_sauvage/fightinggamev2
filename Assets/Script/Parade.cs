﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parade : MonoBehaviour
{
    /// VARIABLES ///
    private Collider2D m_collider;
    public float timeLeft = 1.0f;
    public float countdown = 3.0f;
    public bool parade = false;
    //////
    
    // Start is called before the first frame update
    void Start()
    {
        // Prend le collider de l'objet
        m_collider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // Condition lors de l'appuie sur le bouton "Shift" gauche
        // Si on appuie sur le bouton et que la variable timeLeft est supérieur ou égale à 2,5
        if (Input.GetKeyDown(KeyCode.LeftShift) && timeLeft >= 1.0)
        {
            // on désactive le collider
            m_collider.enabled = !m_collider.enabled;
            parade = true;
        }

        // Condition : Si la variable parade est vrai
            if (parade == true)
            {
                // la valeur de timeLeft diminue
                timeLeft -= Time.deltaTime;
                
                // le gravity scale du prefab du perso 1 est égal à 0
                gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
                
                // Condition : Si la variable timeLeft est inférieur ou égale à 0
                if (timeLeft <= 0)
                {
                    // on active le collider
                    m_collider.enabled = m_collider.enabled;
                    // la variable parade devient fausse
                    parade = false;
                    countdown = 3.0f;
                }
            }

            // Condition : Si la variable parade est fausse
            if (parade == false && timeLeft <= 0)
            {
                // la valeur de countdown diminue
                countdown -= Time.deltaTime;
                
                // le gravity scale du prefab du perso 1 est égal à 1
                gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
                
                // Condition : Si la variable countdown est inférieur ou égale à 0
                if (countdown <= 0)
                {
                    // la valeur de timeLeft est égale à 2,5
                    timeLeft = 1.0f;
                }
            }
    }
}
