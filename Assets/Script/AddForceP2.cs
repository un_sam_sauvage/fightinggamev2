﻿using UnityEngine;

public class AddForceP2 : MonoBehaviour
{
    public float pourcentage;
    public float poids;
    public GameObject healthBarP2;

    private void Start()
    {
        pourcentage = .1f;
        healthBarP2.GetComponent<HealthBar>().textUpdate(pourcentage);
        healthBarP2 = GameObject.FindGameObjectWithTag("HealthBarP2");
    }

    private void Update()
    {
        if (pourcentage <= .1f || pourcentage <0)
        {
            pourcentage = .1f;
        }
    }
    
    public void GetTouch(float force, float direction)
    {    //cette fonction permet de repousser le joueur en fonction de l'attaque qu'a effectué l'adersaire
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-force*pourcentage/poids*direction, 2);
        pourcentage += pourcentage * force/100;
        if (GetComponent<AddForceP2>().enabled)
        {
            healthBarP2 = GameObject.FindGameObjectWithTag("HealthBarP2");
            healthBarP2.GetComponent<HealthBar>().textUpdate(pourcentage);
        }
    }
    public void Heal(float soin)
    {//retire des pourcentage et soigne le joeuur
        pourcentage -= soin;
        if (pourcentage <= .1f || pourcentage <0)
        {
            pourcentage = .1f;
        }
        healthBarP2.GetComponent<HealthBar>().textUpdate(pourcentage);

    }
    
    public void LooseLife()
    {//appelle la fonction qui fait perdre une vie au joueur

        healthBarP2.GetComponent<HealthBar>().textUpdate(0);
        healthBarP2.GetComponent<HealthBar>().OutOfScreen(false);
    }
}
