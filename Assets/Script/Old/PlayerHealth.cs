﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 1;
    public int currentHealth;

    public  SimpleDamageBar damageBar;
    
    private void Start()
    {
        currentHealth = maxHealth;
        damageBar.SetMaxHealth(maxHealth);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            TakeDamage(15);
        }
    }

    public void TakeDamage(int damage)
    {
        currentHealth += damage;
        damageBar.SetHealth(currentHealth);
    }
}
