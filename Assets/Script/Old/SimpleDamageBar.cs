﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimpleDamageBar : MonoBehaviour
{
    public Slider damageBar;
    public Image fill;
    public Gradient gradient;
    
    
    public void SetMaxHealth(int health)
    {
        damageBar.maxValue = health;
        damageBar.value = health;

        fill.color = gradient.Evaluate(1f);
    }
    
    public void SetHealth(int health)
    {
        damageBar.value = health;

        fill.color = gradient.Evaluate(damageBar.normalizedValue);
    }
}

