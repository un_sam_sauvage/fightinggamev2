﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class OutOfTheScreen : MonoBehaviour
{
    public GameObject gears;

    public bool outOftheScreen;
    
    void Start()
    {
        gears = GameObject.FindWithTag("GameController");
    }
    
    void Update()
    {
        //permet de vérifier si le joueur n'est pas sorti de l"écran
        var point = gears.GetComponent<Gears>().cam.ScreenToWorldPoint(new Vector3(gears.GetComponent<Gears>().cam.pixelWidth, gears.GetComponent<Gears>().cam.pixelHeight)); //le points en haut à droite de l'écran dans le monde
        var point2 = gears.GetComponent<Gears>().cam.ScreenToWorldPoint(new Vector3(0, 0)); //le point en bas à gauche de l'écran dans le monde

        if (transform.position.x >= point.x || transform.position.x <= point2.x || transform.position.y >= point.y || transform.position.y <= point2.y) //si l'objet est sortie de l'écran
        {//si le joueur est sorti on va vérifier si il s'agit du joueur 1 ou 2 et lui retirer une vie en conséquence et le fait réapparaître a son point de spawn en remettant ses pourcentages a 0
            /*gears.GetComponent<Gears>().cam.GetComponent<Camera>().transform.DOMove(new Vector3(0, 5, -10), 2);
            gears.GetComponent<Gears>().cam.GetComponent<Camera>().orthographicSize = 15;*/
            
            gears.GetComponent<Gears>().cam.gameObject.transform.position = new Vector3(gears.GetComponent<Gears>().currentMap.centerOfTheStage.x, 
                gears.GetComponent<Gears>().currentMap.centerOfTheStage.y, gears.GetComponent<Gears>().cam.gameObject.transform.position.z);
            
            outOftheScreen = true;
            gears.GetComponent<Gears>().cam.transform.DOShakePosition(1);
            if (gameObject.GetComponent<AddForceP2>().enabled)
            {
                transform.position = gears.GetComponent<Gears>().currentMap.player2Spawn;
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
                gameObject.GetComponent<AddForceP2>().pourcentage = .1f;
                gameObject.GetComponent<AddForceP2>().LooseLife();
            }
            else if (gameObject.GetComponent<AddForceP1>().enabled)
            {
                transform.position = gears.GetComponent<Gears>().currentMap.player1Spawn;
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
                gameObject.GetComponent<AddForceP1>().pourcentage = .1f;
                gameObject.GetComponent<AddForceP1>().LooseLife();
            }
        }
        else
        {
            outOftheScreen = false;
        }
    }
}
