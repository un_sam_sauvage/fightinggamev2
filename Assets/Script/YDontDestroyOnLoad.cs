﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YDontDestroyOnLoad : MonoBehaviour
{
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
}
