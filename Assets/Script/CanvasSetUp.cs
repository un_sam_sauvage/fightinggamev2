﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasSetUp : MonoBehaviour
{
    public GameObject gears;
    
    public Canvas me;
    
    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        me = GetComponent<Canvas>();
        me.worldCamera = gears.GetComponent<Gears>().cam;
    }
    
    void Update()
    {
        
    }
}
